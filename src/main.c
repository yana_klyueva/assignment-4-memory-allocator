#define _DEFAULT_SOURCE

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = REGION_MIN_SIZE}).bytes);
}

int main() {
	print_message("My tests:");

	print_message("--First test--");
	void* heap = heap_init(REGION_MIN_SIZE);
	debug_heap(stdout, heap);
   	print_message("Allocate 4");
    void* block1 = _malloc(1000);
    void* block2 = _malloc(1000);
    void* block3 = _malloc(1000);
    void* block4 = _malloc(1000);
    debug_heap(stdout, heap);
    print_message("Free third");
    _free(block3);
    debug_heap(stdout, heap);
    print_message("Free first");
    _free(block1);
    debug_heap(stdout, heap);
    _free(block2);
    _free(block4);
    heap_free(heap);

    print_message("--Second test--");
    debug_heap(stdout, heap);
    print_message("Allocate BIG block");
    void* block = _malloc(REGION_MIN_SIZE * 3);
    debug_heap(stdout, heap);
    _free(block);
    heap_free(heap);
    
    print_message("--Third test--");
    debug_heap(stdout, heap);
    void* new_memory = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1);
    void* last_block = _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    munmap(new_memory, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    if (last_block == NULL) {
        print_error("Memory wasn't allocate");
    }

    print_message("That's all!");
}